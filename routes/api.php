<?php

use App\Http\Controllers\PostController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/posts', 'PostController@index');
// Route::post('/posts', 'PostController@store');

Route::apiResource('/posts', 'PostController');
Route::apiResource('/roles', 'RoleController');
Route::apiResource('/comments', 'CommentsController');