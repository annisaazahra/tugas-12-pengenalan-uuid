<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //protected $fillable = ['username', 'email', 'name', 'role_id', 'password', 'email_verified_at'];
    protected $guarded = [];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    
    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            //$model->id = Str::uuid();
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }  
        });
    }

    public function role(){
        return $this->belongsTo('App\Roles');
    }

    public function otpCode(){
        return $this->hasOne('App\OtpCode');
    }
}