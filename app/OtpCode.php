<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Users;
use Illuminate\Support\Str;

class OtpCode extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    
    protected static function boot(){
        parent::boot();

        static::creating(function($model){
            //$model->id = Str::uuid();
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }  
        });
    }

    public function user(){
        return $this->belongsTo(Users::class);
    }
}